FROM python:3.8-slim

RUN pip install --upgrade pip

RUN groupadd -r -g 1001 worker
RUN useradd -m -r -u 1001 -g worker worker
WORKDIR /home/worker
USER 1001

COPY --chown=worker:worker requirements.txt requirements.txt
RUN pip install --user -r requirements.txt

ENV PATH="/home/worker/.local/bin:${PATH}"

COPY --chown=worker:worker . .


ENTRYPOINT ["python", "src/main.py"]
