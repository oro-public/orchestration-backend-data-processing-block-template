#!/usr/bin/env python3

import asyncio
from motor.motor_asyncio import AsyncIOMotorClient
from oro_flow_sdk.rabbitmq_block import RabbitMQBlock
import os
import traceback


class SensorDataHandler:  
    def __init__(self, block):
        self.__block = block
        self.__device_cache = {}
        # Start computation loop
        asyncio.get_event_loop().create_task(self.compute_heatmap_and_store())

    def on_message_received(self, message, block):
        # Key format: realm/deviceid/interface/path
        # In our specific case: realm/deviceid/interface/sensorid/co2ppm
        key_tokens = message.get_key().split('/')
        if key_tokens[2] != "com.oronetworks.AirQuality":
            # hmm
            return
        o = {'timestamp': message.get_timestamp(), 'value': message.get_data()}
        device_id = key_tokens[1]
        sensor_id = key_tokens[3]
        if device_id not in self.__device_cache:
            self.__device_cache[device_id] = {}
        if sensor_id not in self.__device_cache[device_id]:
            self.__device_cache[device_id][sensor_id] = o
        else:
            # We want to stream only the max value
            if self.__device_cache[device_id][sensor_id]["value"] < message.get_data():
                self.__device_cache[device_id][sensor_id] = o

    async def compute_heatmap_and_store(self):
        heatmap_interval = self.__block.get_block_config().get(
            "heatmap_interval", 5 * 60)
        mongo_client = AsyncIOMotorClient(
            self.__block.get_block_config()["mongo_url"])
        mongo_db = mongo_client[self.__block.get_block_config()
                                ["mongo_database"]]
        mongo_collection = mongo_db[self.__block.get_block_config()
                                    ["mongo_collection"]]

        while True:
            await asyncio.sleep(heatmap_interval)

            try:
                objects_to_insert = []
                for device_id in self.__device_cache:
                    for sensor_id in self.__device_cache[device_id]:
                        sensor_data = self.__device_cache[device_id][sensor_id]
                        objects_to_insert.append({
                            'device_id':
                            device_id,
                            'sensor_id':
                            sensor_id,
                            'timestamp':
                            sensor_data['timestamp'],
                            "value":
                            sensor_data['value']
                        })

                # Once computation is over, clean up the dictionary
                self.__device_cache.clear()

                # Now, perform MongoDB insertions asynchronously (this way, messages keep flowing)
                result = await mongo_collection.insert_many(objects_to_insert)
                if not result.acknowledged:
                    print("Could not write objects!")
            except Exception:
                print("Exception occurred while handling insertion - moving forward")
                print(traceback.format_exc())


if __name__ == "__main__":
    # Instantiate and start a RabbitMQ Block
    block = RabbitMQBlock("/config/block/config.json",
                          "/config/worker/rabbitmq.conf")
    # Create our handler class
    handler = SensorDataHandler(block)
    # Setup callbacks
    block.set_on_message_received_callback(handler.on_message_received)

    # Run the Block
    block.run()

